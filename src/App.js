import './App.css'
//import {useSelector, useDispatch} from "react-redux";
//import { increment, decrement } from './actions/index';
import ButtonInc from './components/buttonInc';
import ButtonDec from './components/buttonDec';
import Input from './components/input'

function App() {
  // var currentState = useSelector((state)=>state.changeNum)
  // const dispatch = useDispatch();
  return (
    <div className="">
      <div className="Center">
        <h1>
          Redux first-try
        </h1>
        <h2>
          Counter!!
        </h2>
      </div>
      <div className="Center">

        {/* <button onClick={()=>dispatch({type: "DECREMENT"})}><span>-</span></button> */}
        {/* <input name="quantity" type="text" value={currentState.count}/> */}
        {/* <button onClick={()=>dispatch({type: "INCREMENT"})}><span>+</span></button> */}

        {ButtonDec()}
        {Input()}
        {ButtonInc()}

      </div>
    </div>
  );
}

export default App;
