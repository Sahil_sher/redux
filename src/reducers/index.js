import changeNumInc from "./changeInc";
import changeNum from "./changeNum"
import changeNumDec from "./changeDec";
import { combineReducers } from "redux";

const rootReducer = combineReducers({
    //changeNumInc:changeNumInc,
    //changeNumDec:changeNumDec,
    changeNum
})

export default rootReducer;