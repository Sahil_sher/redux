import {useDispatch} from "react-redux";
import '../App.css'

function ButtonInc(){
    const dispatch = useDispatch()
    return(
    <button className="button" onClick={()=>dispatch({type: "INCREMENT"})}><span>+</span></button>
    );
}

export default ButtonInc;