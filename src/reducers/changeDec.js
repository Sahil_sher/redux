import { initialState } from "../actions/index";

const changeNumDec = (state = initialState, action) => {
    switch (action.type){
        case "DECREMENT" : return {
            ...state,
            count : state.count - 1
        }
        default: return state;
    }
}

export default changeNumDec;