import {useDispatch} from "react-redux";
import '../App.css'

function ButtonDec(){
    const dispatch = useDispatch()
    return(
    <button className="button-" onClick={()=>dispatch({type: "DECREMENT"})}><span>-</span></button>
    );
}

export default ButtonDec;